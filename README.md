[[_TOC_]]

# rke-hcloud

Provison a RKE-Cluster on HCLOUD.
## Preparations

* Generate SSH Key
  * ```bash
    ssh-keygen -t ed25519 -C "EMAIL" -f ~/.ssh/user
    ```
* Set Hetzner Cloud Token 
  * ```bash
    export HCLOUD_TOKEN=TOKEN
    ```
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | 1.25.1 |
| <a name="requirement_rke"></a> [rke](#requirement\_rke) | 1.2.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_local"></a> [local](#provider\_local) | n/a |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |
## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_network"></a> [network](#module\_network) | ./modules/network |  |
| <a name="module_rke"></a> [rke](#module\_rke) | ./modules/rke |  |
| <a name="module_server"></a> [server](#module\_server) | ./modules/server |  |
| <a name="module_ssh"></a> [ssh](#module\_ssh) | ./modules/ssh |  |

## Resources

| Name | Type |
|------|------|
| [local_file.kube_cluster_yaml](https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file) | resource |
| [time_sleep.cluster_wait](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment_name"></a> [environment\_name](#input\_environment\_name) | n/a | `string` | `"rke-test"` | no |
| <a name="input_nodes"></a> [nodes](#input\_nodes) | Map of project names to configuration. | `map` | <pre>{<br>  "node01": {<br>    "label": "node",<br>    "server_type": "cpx11"<br>  },<br>  "node02": {<br>    "label": "node",<br>    "server_type": "cpx11"<br>  },<br>  "server01": {<br>    "label": "controlplane",<br>    "server_type": "cx21"<br>  }<br>}</pre> | no |

## Execution
```bash
# add SSH private keys into the SSH authentication agent 
ssh-add
# init proviers and output plan
terraform init && terraform plan
# create hcloud vms & RKE cluster
terraform apply -auto-approve
# destroy the Terraform-managed infrastructure
terraform destroy
```
