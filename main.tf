module "ssh" {
  //source = "./modules/ssh"
  source = "git@gitlab.com:phil.xx/terraform-modules.git//hcloud//ssh?ref=main"
}

module "network" {
  //source = "./modules/network"
  source = "git@gitlab.com:phil.xx/terraform-modules.git//hcloud//network?ref=main"
}

module "server" {
  //source = "./modules/server"
  source = "git@gitlab.com:phil.xx/terraform-modules.git//hcloud//server_cloud_init?ref=main"

  for_each    = var.nodes
  ssh_keys    = [module.ssh.ssh_id]
  server_type = each.value.server_type
  name        = each.key
  network_id  = module.network.network_id
  dependancy  = module.network.subnet_id
  label       = each.value.label
}

resource "time_sleep" "cluster_wait" {
  depends_on = [module.server]

  create_duration = "60s"
}

module "rke" {
  depends_on = [time_sleep.cluster_wait]

  source           = "git@gitlab.com:phil.xx/terraform-modules.git//rke?ref=main"
  nodes            = [for node in module.server : node]
  environment_name = var.environment_name
}

resource "local_file" "kube_cluster_yaml" {
  filename          = "${path.root}/kube_config_cluster.yml"
  sensitive_content = module.rke.cluster["kube_config_yaml"]
}
