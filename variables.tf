variable "environment_name" {
  default = "rke-test"
}

variable "nodes" {
  description = "Map of project names to configuration."
  type        = map(any)
  default = {
    server01 = {
      server_type = "cx21",
      label       = "controlplane"
    },
    node01 = {
      server_type = "cpx11",
      label       = "node"
    }
    node02 = {
      server_type = "cpx11",
      label       = "node"
    }
  }
}
