# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/hashicorp/time" {
  version = "0.7.1"
  hashes = [
    "h1:4Ltywoo5WH0igk1rhr6iKz0fGRXg2iMz5+VXV2oZg2E=",
    "zh:1fa9e5b198db3ae7dcfe70ec505db4c70bf3a557e2dcb6fb19cecbe994fe048f",
    "zh:320d90646638ee4d3fa69ee55bce4d9762ac4ceb0530fe114d1ad183c16ea3fb",
    "zh:6a0150852967abf11975f4cb7a7c4190deb871dcf2cdd7697b4955b789e3de63",
    "zh:6f350e7abf170c97d946ae31fcb8b797eca56f8c46f4052a2ba656931a2ca178",
    "zh:7a24ac6a5e53edeb6d50dd573c02f1b36b96c8a9285f83003481cf537f4d61ea",
    "zh:96c3da650bda44b31ba5513e322fd1902d3cfa9cc99129ede70929c71ca74364",
    "zh:98bd13cf906aa58a4e66493f469eed237569a33ba3016e33784e1d8cbdcd0bd6",
    "zh:98c0053a9bef780653a7234046f30ee0ee628e03db5eeeca2646d155422852bc",
    "zh:a009a597a8b4f14da3a76d7fc52c193535c684d12c7f571ff747ea6c1cf52b9d",
    "zh:b7544277dffba3f2eed74a6440564350f7402a3a24329ea8a3bd6c1fad167683",
    "zh:f1bd5f300260e550086266049f85f4912138d6e89bfe01e1e0ba154696e410b2",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.26.0"
  constraints = "1.26.0"
  hashes = [
    "h1:pdPUtihsFEadqGcMAaCI7gFgNscyEbu8LzzRB/QgGRk=",
    "zh:03d7eb722a4ee25774949baace0125392060d0369d4cb9257d7d298ab6ece3ff",
    "zh:0fed2e63ac4cb6fe6b2a5b6891abf973cb7c1716e487fbabc09216e0ec05e866",
    "zh:1a84c8c1c8e2d6607de5aa09aa3f9254183cde75a5acc666cca5f4b02a1d290e",
    "zh:23ac426aa3a0001fb20045dc35569978864f139732f45ab671c64e80123c91a1",
    "zh:23b78348b24ae3e4679bd90989c999346efd71ee228d17368d5f556f63e5fd06",
    "zh:2503fe28ac87661af96e7755a7404307000822104ac1abc571271eee46c95ab5",
    "zh:3fe859b2611d20ed5cd65cc2ec812acf73c7dfb39f2fee45ef99a3896c2662a8",
    "zh:51ef869ed35d0d8aada35f587c4a64802f1140dc93c40a4e7c9800560143bb1a",
    "zh:69b93cf4adca465b89da08e4e3b4aaf831821f1fbae68e526c0a292b3cfa463d",
    "zh:6a4e23c6aa86e3d30240e6e4c97daef3af9ad217be2c6f35300fe1839fdbf8b2",
    "zh:97a513459692a981a62b4a566c1d736c4a67622d2fbbee3771ec3ea8d576d484",
    "zh:fec6c07731e23d1dd45015b44747b89c4fee58b5b2560f96d24c7da5a8ecb2ad",
  ]
}

provider "registry.terraform.io/rancher/rke" {
  version     = "1.2.1"
  constraints = "1.2.1"
  hashes = [
    "h1:kPe8pDvdVlW6OGKGWMtMS8ddl6zdimyFtR69r+2dUa8=",
    "zh:49a06fdcbec632665175716defe37e7d9867e539c34cb27e0f36fc3b269f2252",
    "zh:afb4341cf562fccee62509cf61b2eb8889c6666516d745371c1903b672c366ae",
    "zh:c3d7e0bd1a2afb129f293ad9b78b727ff188404445c50abe734f105f2e7d62e0",
    "zh:d9a0bd876a575c21115a58c177f601eb1cde4c11af0f9be8f7d6f024cc89bfae",
    "zh:e1f8bd3a740322eb0e669b08543760db25d5754a703f5773ece50d533d7d57f2",
    "zh:e5f1f4a7d33da5834024fab3c4949e7852afcb99ffa62d5702017309ca661ea7",
  ]
}
